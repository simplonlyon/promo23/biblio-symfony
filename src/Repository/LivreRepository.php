<?php

namespace App\Repository;

use App\Entities\Livre;
use PDO;


class LivreRepository
{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Livre
     * @return Livre[]
     */
    public function findAll(): array
    {
        /** @var Livre[] */
        $livres = [];


        $statement = $this->connection->prepare('SELECT * FROM livres');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $livres[] = $this->sqlToLivre($item);
        }
        return $livres;
    }

    /**
     * Méthode permettant de faire persister une instance de Livre sur la base de données
     * @param Livre $livre le chien à faire persister
     * @return void Aucun retour, mais une fois persisté, le chien aura un id assigné
     */
    public function persist(Livre $livre) {
        $statement = $this->connection->prepare('INSERT INTO livres (titre,auteur,dispo) VALUES (:titre, :auteur, :dispo)');
        $statement->bindValue('titre', $livre->getTitre());
        $statement->bindValue('auteur', $livre->getAuteur());
        $statement->bindValue('dispo',  $livre->getDispo());

        $statement->execute();

        $livre->setId($this->connection->lastInsertId());

    }
    public function update(Livre $livre) {
        $statement = $this->connection->prepare('UPDATE livres SET name=:name,breed=:breed,birthdate=:birthdate WHERE id=:id');
        $statement->bindValue('titre', $livre->getTitre());
        $statement->bindValue('auteur', $livre->getAuteur());
        $statement->bindValue('dispo', $livre->getDispo());
        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);


        $statement->execute();

    }
    public function delete(Livre $livre) {
        $statement = $this->connection->prepare('DELETE FROM livres WHERE id=:id');
        $statement->bindValue('id', $livre->getId(), PDO::PARAM_INT);


        $statement->execute();

    }

    /**
     * Méthode qui va renvoyer un Livre en se basant sur son id, ou null si l'id ne
     * correspond à aucun chien en bdd
     * @param int $id l'id du chien qu'on cherche
     * @return Livre|null L'instance de chien correspondante, ou null si pas de correspondance
     */
    public function findById(int $id):?Livre {
        $statement = $this->connection->prepare('SELECT * FROM livres WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToLivre($result);
        }
        return null;
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Livre
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Livre l'instance de chien
     */
    private function sqlToLivre(array $line):Livre {
       
        //ou bien avec un tertiaire
        //$birthdate = isset($line['birthdate']) ? new DateTime($line['birthdate']):null;
        return new Livre($line['titre'], $line['auteur'], $line['dispo'], $line['id']);
    }
}