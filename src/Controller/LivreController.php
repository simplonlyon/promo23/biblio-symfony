<?php

namespace App\Controller;
use App\Entities\Livre;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * On peut définir une route globale sur le contrôleur directement, ici toutes les routes
 * de ce contrôleur seront préfixées par /api/livre
 */
#[Route('/api/livre')]
class LivreController extends AbstractController {
    private LivreRepository $repo;
    /**
     * On utilise ici l'injection de dépendance de Symfony qui va, en se basant sur le type de
     * l'argument, se charger de créer une instance de la classe voulue
     * On peut aussi faire de l'injection dans les méthodes directement, chose qu'on fait en dessous
     * par exemple avec la Request et le SerializerInterface (ces deux classes spécifiques, on ne sait
     * même pas trop comment en faire des instances nous même, mais Symfony lui le sait, et s'en charge)
     */
    public function __construct(LivreRepository $repo) {
    	$this->repo = $repo;
    }
    /**
     * On indique ici que cette route n'est accessible que via une méthode GET, pas la peine de
     * répéter l'url grâce à la route globale du contrôleur
     */
    #[Route(methods: 'GET')]
    public function all() {
        
        $livres = $this->repo->findAll();
        return $this->json($livres);
    }
    /**
     * Cette route va utiliser le paramètre id (sur la route /api/livre/1 par exemple, en GET) pour
     * récupérer le chien correspondant dans le repository, si celui ci n'existe pas, on throw une NotFoundException
     * qui permettra à symfony de généré une erreur 404
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $livre = $this->repo->findById($id);
        if(!$livre){

            throw new NotFoundHttpException();

        }
        return $this->json($livre);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        
        $livre = $serializer->deserialize($request->getContent(), Livre::class, 'json');
        $this->repo->persist($livre);

        return $this->json($livre, Response::HTTP_CREATED);
    }


  
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $livre = $this->repo->findById($id);
        if(!$livre){
            throw new NotFoundHttpException();
        }
        $errors = $validator->validate($livre);

        if($errors->count() > 0) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }


        $toUpdate = $serializer->deserialize($request->getContent(), Livre::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
        
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $livre = $this->repo->findById($id);

        if(!$livre){
            throw new NotFoundHttpException();
        }

        $this->repo->delete($livre);

        return $this->json(null, Response::HTTP_NO_CONTENT);
        
    }


}